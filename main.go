package main

import (
	"github.com/gorilla/mux"
	"gitlab.com/ealain/cactus-backend/api"
	"log"
	"net/http"
)

func main() {

	// Routes
	r := mux.NewRouter()
	r.HandleFunc("/api/games", api.GetGames).Methods("GET")
	r.HandleFunc("/api/games", api.PostGames).Methods("POST")
	r.HandleFunc("/api/games/{id}", api.GetGame).Methods("GET")
	r.HandleFunc("/api/games/{id}", api.PutGame).Methods("PUT")
	r.HandleFunc("/api/games/{id}/events", api.PostGameEvents).Methods("POST")
	r.HandleFunc("/api/games/{gid}/players", api.PostPlayers).Methods("POST")
	r.HandleFunc("/api/games/{gid}/players", api.GetPlayers).Methods("GET")
	r.HandleFunc("/api/games/{gid}/players/self", api.GetSelf).Methods("GET")
	r.HandleFunc("/api/games/{gid}/players/{id}", api.GetPlayer).Methods("GET")
	r.HandleFunc("/api/games/{gid}/players/{id}", api.PutPlayer).Methods("PUT")

	// Start the web server
	srv := &http.Server{
		Handler: r,
		Addr:    ":8081",
	}
	log.Panic(srv.ListenAndServe())

}
