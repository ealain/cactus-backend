package player

import (
	"encoding/base32"
	"errors"
	"github.com/gorilla/websocket"
	"gitlab.com/ealain/cactus-backend/game"
	"math/rand"
	"time"
)

var clients = make(map[string]*client)

var ErrNoPlayerFound = errors.New("player: not found")

var players = make(map[string]*Player)

func init() {
	rand.Seed(time.Now().UnixNano())
}

type Player struct {
	Id       string `json:"id"`
	GameId   string `json:"gameId"`
	Hand     []int  `json:"hand"`
	Name     string `json:"name"`
	Position int    `json:"position"`
}

type client struct {
	gameId string
	ws     *websocket.Conn
	events chan []byte
}

// Notify sends dummy bytes to the socket of the clients of a given game
func Notify(gameId string) {
	for _, c := range clients {
		if c.gameId == gameId {
			c.events <- make([]byte, 1)
		}
	}
}

func NewPlayer(gameId string, playerId string) (np *Player, err error) {

	if p, ok := players[playerId]; ok && p.GameId == gameId {
		return np, errors.New("This resource already exists.")
	}

	bits := make([]byte, 5)
	rand.Read(bits)
	id := base32.StdEncoding.EncodeToString(bits)

	g, err := game.ById(gameId)
	if err == game.ErrNoGameFound {
		return np, err
	}

	// Draw the cards from the hidden stack
	hand := g.Draw(4)

	players[id] = &Player{
		Id:       id,
		GameId:   gameId,
		Hand:     hand,
		Name:     "",
		Position: len(ByGame(gameId)),
	}

	Notify(g.Id)

	return players[id], nil
}

func Update(u Player) (p *Player, err error) {

	// Get a reference to the player and check the game ID consistency
	if p, ok := players[u.Id]; ok && p.GameId == u.GameId {
		p.Name = u.Name
		return p, nil
	}

	return nil, errors.New("")
}

func ByGame(gameId string) (pp []*Player) {
	for _, p := range players {
		if p.GameId == gameId {
			pp = append(pp, p)
		}
	}
	return pp
}

func ById(id string) (p *Player, err error) {
	if p, ok := players[id]; ok {
		return p, nil
	}
	return nil, ErrNoPlayerFound

}

func (p *Player) SetClient(ws *websocket.Conn) {
	clients[p.Id] = &client{gameId: p.GameId, ws: ws, events: make(chan []byte)}
	go clients[p.Id].pumpEvents()
	go clients[p.Id].readLoop()
}

func (c *client) pumpEvents() {
	defer c.ws.Close()
	// Wait for new events
	for e := range c.events {
		c.ws.WriteMessage(websocket.TextMessage, e)
	}
}

// readLoop reads the connection to process close, ping and pong messages
// sent from the peer
func (c *client) readLoop() {
	for {
		if _, _, err := c.ws.NextReader(); err != nil {
			c.ws.Close()
			break
		}
	}
}
