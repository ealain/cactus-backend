package game

import "testing"

func TestAll(t *testing.T) {
    g1 := &game{Id:"GAME1"}
    g2 := &game{Id:"GAME2"}
    games["GAME1"] = g1
    games["GAME2"] = g2
    gg := All()
    if gg[0] != g1 || gg[1] != g2 {
        t.Error()
    }
}
