package game

import (
	"encoding/json"
	"errors"
	"fmt"
)

func Process(t string, gameId string, playerId string) (r json.RawMessage, err error) {
	switch t {
	case "draw":
		g, err := ById(gameId)
		if err != nil {
			return nil, err
		}
		return json.RawMessage(fmt.Sprintf(`{"card": %d}`, g.Draw(1)[0])), nil
	}
	return nil, errors.New("Unknown event")
}
