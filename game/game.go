package game

import (
	"encoding/base32"
	"errors"
	"math/rand"
	"time"
)

// ErrNoGameFound indicates that a game with the provided ID does not exist
var ErrNoGameFound = errors.New("game: no game found")

// games stores the games in memory in the absence of a database
var games = make(map[string]*Game)

// init sets the rand seed as the current time
func init() {
	rand.Seed(time.Now().UnixNano())
}

type Game struct {
	Id    string `json:"id"`
	stack []int
	Stack []int `json:"stack"`
	Turn  int   `json:"turn"`
}

// Draw draws the card on top of the hidden stack and returns them
// TODO: when there is no more card in the hidden stack, use the shuffled
// visible stack
func (g *Game) Draw(n int) (d []int) {
	d = g.stack[:n]
	g.stack = g.stack[n:]
	return d
}

// IsStarted returns true if the game has started
func (g *Game) IsStarted() bool {
	return g.Turn >= 0
}

// Update updates the stack and turn
func (g *Game) Update(u Game) {
	g.Stack = u.Stack
	g.Turn = u.Turn
}

// All returns all the games in memory
func All() []*Game {
	gg := make([]*Game, len(games))
	i := 0
	for _, g := range games {
		gg[i] = g
		i++
	}
	return gg
}

// ById returns the game with the ID and nil or ErrNoGameFound if no
// such game exists in memory
func ById(id string) (g *Game, err error) {
	if g, ok := games[id]; ok {
		return g, nil
	} else {
		return nil, ErrNoGameFound
	}
}

// New creates a game
//   - generates an ID
//   - initializes the stacks
func New() *Game {
	// Generate a random ID
	id := make([]byte, 5)
	rand.Read(id)

	// Shuffle the cards
	h_stack := rand.Perm(52)

	// Draw the first card and place it on the visible stack
	v_stack := make([]int, 1, 52)
	v_stack[0] = h_stack[0]
	h_stack = h_stack[1:]

	g := &Game{
		Id:    base32.StdEncoding.EncodeToString(id),
		stack: h_stack,
		Stack: v_stack,
		Turn:  -1,
	}
	games[g.Id] = g

	return g
}
