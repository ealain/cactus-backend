package api

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/ealain/cactus-backend/game"
	"gitlab.com/ealain/cactus-backend/player"
	"log"
	"net/http"
)

func GetGames(w http.ResponseWriter, r *http.Request) {
	gg := game.All()
	j, _ := json.Marshal(gg)
	fmt.Fprint(w, string(j))
}

func PostGames(w http.ResponseWriter, r *http.Request) {
	g := game.New()

	j, _ := json.Marshal(g)

	w.WriteHeader(http.StatusCreated)
	fmt.Fprint(w, string(j))
}

func PutGame(w http.ResponseWriter, r *http.Request) {
	fct := "putGame"

	id := mux.Vars(r)["id"]

	var body game.Game
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		msg := "error decoding json"
		log.Println(fct, msg, err)
		return
	}

	g, err := game.ById(id)
	if err == game.ErrNoGameFound {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	g.Update(body)

	player.Notify(g.Id)

	j, _ := json.Marshal(g)
	fmt.Fprint(w, string(j))
}

func GetGame(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	g, err := game.ById(id)
	if err == game.ErrNoGameFound {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	j, err := json.Marshal(g)
	if err != nil {
		msg := "JSON marshalling failed"
		log.Println(msg, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	fmt.Fprint(w, string(j))
}

func PostGameEvents(w http.ResponseWriter, r *http.Request) {
	fct := "postGameEvents"

	id := mux.Vars(r)["id"]

	cookie, err := r.Cookie("pid")
	if err != nil {
		msg := "error decoding cookie"
		log.Println(fct, msg, err)
		return
	}

	var b struct {
		T string `json:"t"`
	}
	err = json.NewDecoder(r.Body).Decode(&b)
	if err != nil {
		msg := "error decoding json"
		log.Println(fct, msg, err)
		return
	}

	res, err := game.Process(b.T, id, cookie.Value)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	j, err := res.MarshalJSON()
	fmt.Fprint(w, string(j))
}
