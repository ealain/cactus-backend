package api

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"gitlab.com/ealain/cactus-backend/game"
	"gitlab.com/ealain/cactus-backend/player"
	"log"
	"net/http"
)

// PostPlayers
// POST /api/games/{gid}/players
// PostPlayers:
//   - creates a player if it does not exist (201)
//   - returns (403) if it already exists or if the game has started
//   - returns (404) if the game does not exist
func PostPlayers(w http.ResponseWriter, r *http.Request) {
	gid := mux.Vars(r)["gid"]

	if g, err := game.ById(gid); err == nil && g.IsStarted() {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprint(w, `{"msg":"The game has already started"}`)
		return
	}

	cc, err := r.Cookie("pid")
	var cookie string
	if err == nil {
		cookie = cc.Value
	}

	p, err := player.NewPlayer(gid, cookie)
	if err == game.ErrNoGameFound {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if err != nil {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprint(w, fmt.Sprintf(`{"msg":%s}`, err))
		return
	}

	c := &http.Cookie{
		Name:     "pid",
		Value:    p.Id,
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	}
	http.SetCookie(w, c)
	w.WriteHeader(http.StatusCreated)

	j, _ := json.Marshal(p)
	fmt.Fprint(w, string(j))
	return
}

// PutPlayer
// PUT /api/games/{gid}/players/{id}
// PutPlayer updates a player
func PutPlayer(w http.ResponseWriter, r *http.Request) {
	// Deserialize the body of the request
	var body player.Player
	json.NewDecoder(r.Body).Decode(&body)

	// Get the player ID from the cookie, if any
	cc, err := r.Cookie("pid")
	if err == nil {
		body.Id = cc.Value
	}

	// Check that the game of the player exists
	g, err := game.ById(body.GameId)
	if err == game.ErrNoGameFound {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	// Update the player
	p, err := player.Update(body)
	if p == nil || err != nil {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	player.Notify(g.Id)

	c := &http.Cookie{
		Name:     "pid",
		Value:    p.Id,
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	}
	http.SetCookie(w, c)

	j, _ := json.Marshal(p)
	fmt.Fprint(w, string(j))
	return
}

// GetPlayers
// GET /api/games/{gid}/players
// GetPlayers returns the players of the game
func GetPlayers(w http.ResponseWriter, r *http.Request) {
	gid := mux.Vars(r)["gid"]

	pp := player.ByGame(gid)

	type p struct {
		Name     string `json:"name"`
		Position int    `json:"position"`
	}
	ppp := make([]p, len(pp))
	for i := range pp {
		ppp[i] = p{
			Name:     pp[i].Name,
			Position: pp[i].Position,
		}
	}

	j, _ := json.Marshal(ppp)
	fmt.Fprint(w, string(j))
	return
}

// GetPlayer
// GET /api/games/{gid}/players/{id}
// GetPlayer:
//   - upgrades the connection to a web socket
//   - registers a client
func GetPlayer(w http.ResponseWriter, r *http.Request) {
	fct := "GetPlayer"
	id := mux.Vars(r)["id"]
	gid := mux.Vars(r)["gid"]

	p, err := player.ById(id)
	if err == player.ErrNoPlayerFound || p.GameId != gid {
		log.Println(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	// Open a web socket
	var upgrader websocket.Upgrader
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		msg := "unable to upgrade to WS"
		log.Println(fct, msg, err)
		return
	}

	// Register a new client
	p.SetClient(ws)
}

// GetSelf
// GET /api/games/{gid}/players/self
// GetSelf extracts the player ID from the cookie and returns:
//   - 200 with the player if it exists for the given game
//   - 404 otherwise (no cookie, or no player)
func GetSelf(w http.ResponseWriter, r *http.Request) {
	gid := mux.Vars(r)["gid"]

	cc, err := r.Cookie("pid")
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	p, err := player.ById(cc.Value)
	if err == player.ErrNoPlayerFound || p.GameId != gid {
		log.Println(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	j, _ := json.Marshal(p)
	fmt.Fprint(w, string(j))
	return
}
