module gitlab.com/ealain/cactus-backend

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2
	github.com/lib/pq v1.5.1
)
